package com.example.mylittlegreenthumb.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.mylittlegreenthumb.models.LoginResponse
import com.example.mylittlegreenthumb.R
import com.example.mylittlegreenthumb.api.RetrofitClient
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        progressBarMainActivityLogin.visibility = View.GONE


        buttonLoginActivity.setOnClickListener {
            val email = editTextEmailloginActivity.text.toString().trim()
            val password = editTextTextPasswordLoginMainActivity.text.toString().trim()
            val usertype:Int = 1

            if(email.isEmpty()){
                editTextEmailloginActivity.error = "Email required"
                editTextEmailloginActivity.requestFocus()
                return@setOnClickListener

            }


            if(password.isEmpty()){
                editTextTextPasswordLoginMainActivity.error = "Password required"
                editTextTextPasswordLoginMainActivity.requestFocus()
                return@setOnClickListener
            }
            showLoading()
            if (isNetworkConnected()) {

                RetrofitClient.instance.loginUser(email, password, usertype)
                    .enqueue(object : Callback<LoginResponse> {


                        override fun onResponse(
                            call: Call<LoginResponse>,
                            response: Response<LoginResponse>
                        ) {
                            if (response.body()?.status == "success") {
                                Toast.makeText(
                                    applicationContext,
                                    "Login Success",
                                    Toast.LENGTH_LONG
                                ).show()


                                startActivity(
                                    Intent(
                                        this@MainActivity,
                                        HomeActivity::class.java
                                    )
                                )
                                hideLoading()
                            } else if (response.body()?.status == "error") {
                                Toast.makeText(applicationContext, "Error", Toast.LENGTH_LONG)
                                    .show()
                                hideLoading()
                            } else {
                                Toast.makeText(this@MainActivity, "Error", Toast.LENGTH_SHORT)
                                    .show()
                                hideLoading()
                            }
                        }

                        override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                            Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
                            hideLoading()
                        }

                    })
            }
            else{
                hideLoading()
                Toast.makeText(this,"Connect to internet",Toast.LENGTH_LONG).show()
            }
        }
    }
    override fun onBackPressed() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

}