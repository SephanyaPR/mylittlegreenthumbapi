package com.example.mylittlegreenthumb.models

import com.example.mylittlegreenthumb.models.User

data class LoginResponse(val status:String, val message:String, val data: User)